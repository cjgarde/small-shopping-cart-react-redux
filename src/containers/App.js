import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ITEMS from '../utils/constants';
import items from '../store/state/items';
import MarketItems from '../components/Items';
import ShoppingCart from '../components/ShoppingCart';
import './App.css';

/**
 * Main class that is a Container. The only class connected to
 * the Store so the only one that have access to the app state
 * and will send it to the children (components) via props
 */
class App extends Component {
	static propTypes = {
		items: PropTypes.object.isRequired,
		addItem: PropTypes.func.isRequired,
		decreaseItem: PropTypes.func.isRequired,
		resetItem: PropTypes.func.isRequired,
		setItemPrice: PropTypes.func.isRequired,
		setOfferActivated: PropTypes.func.isRequired
	}

	constructor(props) {
		super(props);
		// initialize the items in the store and its prices regarding
		// the info in the constants file
		this.items = Object.keys(ITEMS).map((item) => item);
		this.items.forEach((item) => {
			this.props.setItemPrice(item, ITEMS[item].PRICE)
				.then(() => {
					ITEMS[item].OFFER && this.props.setOfferActivated(item, false);
				});
		});
		// initialize state to send totalPrice to child component
		this.state = {
			totalPrice: 0
		}
	}

	/**
	 * Set in the STORE if an item has the offer 3x2 activated
	 * @param {string} item - item that we are checking
	 * @private
	 */
	_has3x2Offer = function(item) {
		if(ITEMS[item].OFFER &&	ITEMS[item].OFFER === "3x2") {
			this.props.items[item].qty >= 2 ?
			this.props.setOfferActivated(item, true) :
				this.props.setOfferActivated(item, false);
		}
		else {
			this.props.setOfferActivated(item, false);
		}
	}

	/**
	 * Will trigger the action creator to add an item and will update
	 * the container state because the shopping cart
	 * @param {string} item - One of the available items in the APP
	 */
	addItem = (item) => {
		this.props.addItem(item)
			.then(() => {
				this._has3x2Offer(item);
				this.setState({
					totalPrice: +(this.state.totalPrice + this.props.items[item].price).toFixed(2)
				});
			});
	}

	/**
	 * Will trigger the action creator to decrease an item and will update
	 * the container state because the shopping cart
	 * @param {string} item - One of the available items in the APP
	 */
	decreaseItem = (item) => {
		this.props.decreaseItem(item)
			.then(() => {
				this._has3x2Offer(item);
				this.setState({
					totalPrice: +(this.state.totalPrice - this.props.items[item].price).toFixed(2)
				});
			});
	}

	/**
	 * Will reset to zero the selected quantity of an item and will update
	 * the container state because the shopping cart
	 * @param {string} item - One of the available items in the APP
	 */
	resetItem = (item) => {
		let totalPrice = this.state.totalPrice;

		totalPrice -= +(this.props.items[item].qty * this.props.items[item].price).toFixed(2);
		this.setState({
			totalPrice
		});
		this.props.resetItem(item);
	}

	render() {
		return (
		  <div className="App">
			<div className="App-header">
			  <h2>Really small shopping cart</h2>
			</div>
			<div className="content">
				<MarketItems
					items={this.props.items}
					addItem={this.addItem}
					decreaseItem={this.decreaseItem}
					resetItem={this.resetItem} />
				<ShoppingCart
					items={this.props.items}
					totalPrice={this.state.totalPrice} />
			</div>
		  </div>
		);
	}
}

const mapStateToProps = (state) => ({
		items: items.selectors.getItem(state)
	}),
	mapDispatchToProps = {
		addItem: items.actions.addUnit,
		decreaseItem: items.actions.decreaseUnit,
		resetItem: items.actions.resetToZero,
		setItemPrice: items.actions.setPrice,
		setOfferActivated: items.actions.setOfferActivated
	};

export default connect(mapStateToProps, mapDispatchToProps)(App);