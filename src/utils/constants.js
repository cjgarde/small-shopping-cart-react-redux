const ITEMS = {
	APPLES: {
		PRICE: 0.25
	},
	ORANGES :{
		PRICE: 0.3
	},
	BANANAS: {
		PRICE: 0.15
	},
	PAPAYAS: {
		PRICE: 0.50,
		OFFER: "3x2"
	}
}
export default ITEMS;