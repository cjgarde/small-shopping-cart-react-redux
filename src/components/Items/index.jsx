import React, { PureComponent } from 'react';
import './Items.css';
import PropTypes from 'prop-types';
import ITEMS from '../../utils/constants';
import { Col } from 'react-bootstrap';
import Card from '../Card';

/**
 * Class that will show some cards with the available items
 * Any callback (Action) is passed via props from the container
 */
class Items extends PureComponent {
	static propTypes = {
		items: PropTypes.object.isRequired,
		addItem: PropTypes.func.isRequired,
		decreaseItem: PropTypes.func.isRequired,
		resetItem: PropTypes.func.isRequired
	};

	/**
	 * With the list of available items received in the constants file
	 * compose a card with price, name and buttons to add, decrease or reset
	 * @returns {Array} - with the cards to render
	 * @private
	 */
	_getItems = function() {
		const itemsLength = Object.keys(ITEMS).length;
		const cards = [];

		for(let i=0; i<itemsLength; i++) {
			const item = Object.keys(ITEMS)[i];
			const hasOffer = ITEMS[item].OFFER || '';

			cards.push(
				<Card
					item={item}
					hasOffer={hasOffer}
					price={this.props.items[item].price}
					qty={this.props.items[item].qty}
					addItem={ this.props.addItem }
					removeItem={ this.removeItem }
					resetItem={ this.props.resetItem }
					key={i} />
			)
		}

		return cards;
	}

	/**
	 * Only call the action if we have more than zeo items selected
	 * @param {string} item - one of the available items
	 */
	removeItem = (item) => {
		this.props.items[item].qty > 0 && this.props.decreaseItem(item);
	}

	render() {
		return(
			<Col className="items">
				{this._getItems()}
			</Col>
		)
	}
}

export default Items;