import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Col, Label, Row } from 'react-bootstrap';
import MiniButton from '../Button';

/**
 * Component that create a Card with information received from an item
 */
class Card extends PureComponent {
	static propTypes = {
		hasOffer: PropTypes.string.isRequired,
		item: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
		qty: PropTypes.number.isRequired,
		addItem: PropTypes.func.isRequired,
		removeItem: PropTypes.func.isRequired,
		resetItem: PropTypes.func.isRequired
	}

	render() {
		return(
			<Col className="itemInfo">
				{ this.props.hasOffer && <div className="corner-ribbon">{this.props.hasOffer}</div>}
				<Row className="itemName">
					<h2>{this.props.item}</h2>
				</Row>
				<Row className="itemUnitPrice">
					Unit price: {this.props.price}
				</Row>
				<Row>
					<MiniButton text="-1" onClick={() => this.props.removeItem(this.props.item)} />
					<Label className="itemTotalQty">{this.props.qty}</Label>
					<MiniButton text="+1" onClick={() => this.props.addItem(this.props.item)} />
				</Row>
				<Row className="itemButtonReset">
					<MiniButton text="Reset to 0" onClick={() => this.props.resetItem(this.props.item)} />
				</Row>
			</Col>
		)
	}
}

export default Card;