import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'react-bootstrap';

/**
 * Component that render a button and set the callback received via props
 * to the onClick event and render the text also received via props
 */
export default class MiniButton extends PureComponent {
	static propTypes = {
		onClick: PropTypes.func.isRequired,
		text: PropTypes.string.isRequired
	}

	_onClick = (e) => {
		e.preventDefault();
		this.props.onClick();
	}

	render() {
		return (
			<Button onClick= {this._onClick}>
				{this.props.text}
			</Button>
		)
	}
}