import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Row, Table } from 'react-bootstrap';
import './ShoppingCart.css';

/**
 * This class is a component that will receive the items to show and total number
 * to render and show a table as a receipt
 */
class ShoppingCart extends PureComponent {
	static propTypes = {
		items: PropTypes.object.isRequired,
		totalPrice: PropTypes.number.isRequired
	}

	/**
	 * Return the rows with the updated information of items, quantity and price
	 * @returns {Array} - with objects with the rows
	 * @private
	 */
	_createRows = function() {
		const rows = [];

		Object.keys(this.props.items).forEach((item, index) => {
			if (this.props.items[item].qty > 0) {
				rows.push(
					<tr key={index}>
						<th>
							<span>
								{this.props.items[item].qty} un. of <span className="itemName">{item}</span> @ {this.props.items[item].price.toFixed(2)} each
							</span>
							<p className="itemTotalPrice">
								{+(this.props.items[item].qty * this.props.items[item].price).toFixed(2)}
							</p>
							{ this.props.items[item].activatedOffer && (
								<p className="itemWithOffer">
									{Math.trunc(this.props.items[item].qty/2)} more FREE units! ("3x2" offer applied)
								</p>
							)}
						</th>
					</tr>
				)
			}
		});

		return rows;
	}

	render() {
		return(
			<Row className="shoppingCart">
				<div className="shoppingCartTitle">
					<h1>Receipt</h1>
				</div>
				<Table responsive>
					<tbody>
						{ this._createRows() }
					</tbody>
				</Table>
				<div className="shoppingCartTotal">
					<h2>Total Price: {+(this.props.totalPrice).toFixed(2)}</h2>
				</div>
			</Row>

		);
	}
}

export default ShoppingCart;