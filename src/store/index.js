import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import rootReducers from './state/rootReducer';

const initialState = {};
const enhancers = [];
const middleware = [thunk];
const composedEnhancers = composeWithDevTools(
	applyMiddleware(...middleware),
	...enhancers
)
const store = createStore(rootReducers, initialState, composedEnhancers);
export default store;