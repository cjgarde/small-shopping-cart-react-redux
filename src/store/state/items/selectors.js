const getItem = (state) => state.items.toJS();

export { getItem };