import { ADD_ITEM_QTY, DECREASE_ITEM_QTY, RESET_ITEM_QTY, SET_ITEM_PRICE, SET_ACTIVATED_OFFER } from "../constants";
import { fromJS } from "immutable";

const initialState = fromJS({
	APPLES: { qty:0, price:0 },
	BANANAS: { qty:0, price:0 },
	ORANGES: { qty:0, price:0 },
	PAPAYAS: { qty:0, price:0 }
});

export default (state = initialState, action) => {
	let newQty;
	let item;

	switch (action.type) {
		case ADD_ITEM_QTY:
			newQty = state.get(action.payload.item).get("qty") + 1;
			item = state.get(action.payload.item).mergeDeep({ qty: newQty });
			return state.merge({
				[action.payload.item]: item
			});
		case DECREASE_ITEM_QTY:
			newQty = state.get(action.payload.item).get("qty") - 1;
			item = state.get(action.payload.item).mergeDeep({ qty: newQty });
			return state.merge({
				[action.payload.item]: item
			});
		case RESET_ITEM_QTY:
			item = state.get(action.payload.item).mergeDeep({ qty: 0 });
			return state.merge({
				[action.payload.item]: item
			});
		case SET_ITEM_PRICE:
			item = state.get(action.payload.item).mergeDeep({ price: action.payload.price });
			return state.merge({
				[action.payload.item]: item
			});
		case SET_ACTIVATED_OFFER:
			item = state.get(action.payload.item).mergeDeep({ activatedOffer: action.payload.activatedOffer });
			return state.merge({
				[action.payload.item]: item
			});
		default:
			return state;
	}
};