import * as CONSTANTS from './constants';

export const addUnit = (item) => {
	return dispatch => {
		dispatch({
			type: CONSTANTS.ADD_ITEM_QTY,
			payload: {
				item
			}
		});
		return Promise.resolve();
	}
}

export const decreaseUnit = (item) => {
	return dispatch => {
		dispatch({
			type: CONSTANTS.DECREASE_ITEM_QTY,
			payload: {
				item
			}
		});
		return Promise.resolve();
	}
};

export const resetToZero = (item) => {
	return dispatch => {
		dispatch({
			type: CONSTANTS.RESET_ITEM_QTY,
			payload: {
				item
			}
		});
		return Promise.resolve();
	}
};

export const setPrice = (item, price) => {
	return dispatch => {
		dispatch({
			type: CONSTANTS.SET_ITEM_PRICE,
			payload: {
				item,
				price
			}
		})
		return Promise.resolve();
	}
};

export const setOfferActivated = (item, activatedOffer) => {
	return dispatch => {
		dispatch({
			type: CONSTANTS.SET_ACTIVATED_OFFER,
			payload: {
				item,
				activatedOffer
			}
		})
		return Promise.resolve();
	}
};