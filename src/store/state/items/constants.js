export const NAME = "items";
export const ADD_ITEM_QTY = `${NAME}/ADD_ITEM_QTY`;
export const DECREASE_ITEM_QTY = `${NAME}/DECREASE_ITEM_QTY`;
export const RESET_ITEM_QTY = `${NAME}/RESET_ITEM_QTY`;
export const SET_ITEM_PRICE = `${NAME}/SET_ITEM_PRICE`;
export const SET_ACTIVATED_OFFER = `${NAME}/SET_ACTIVATED_OFFER`;

