import items from './items';
import { combineReducers } from 'redux';

// the children projects (which are extending the reducers)
// have the responsibility to set the reducer with the combineReducer func
// so for this, here we only export the reducers and not combine them yet
const reducers = {
	items: items.reducer
};

export default combineReducers(reducers);